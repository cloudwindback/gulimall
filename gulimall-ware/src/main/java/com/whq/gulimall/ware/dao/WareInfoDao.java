package com.whq.gulimall.ware.dao;

import com.whq.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 14:38:41
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
