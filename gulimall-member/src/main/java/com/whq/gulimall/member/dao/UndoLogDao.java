package com.whq.gulimall.member.dao;

import com.whq.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 11:27:37
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
