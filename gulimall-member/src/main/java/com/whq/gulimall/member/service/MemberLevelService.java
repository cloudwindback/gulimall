package com.whq.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 11:27:37
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

