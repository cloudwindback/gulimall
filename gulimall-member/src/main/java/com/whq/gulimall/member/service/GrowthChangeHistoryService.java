package com.whq.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.member.entity.GrowthChangeHistoryEntity;

import java.util.Map;

/**
 * 成长值变化历史记录
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 11:27:37
 */
public interface GrowthChangeHistoryService extends IService<GrowthChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

