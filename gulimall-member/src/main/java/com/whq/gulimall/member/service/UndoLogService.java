package com.whq.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.member.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 11:27:37
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

