package com.whq.gulimall.member.dao;

import com.whq.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 11:27:37
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
