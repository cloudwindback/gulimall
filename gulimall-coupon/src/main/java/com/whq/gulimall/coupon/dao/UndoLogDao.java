package com.whq.gulimall.coupon.dao;

import com.whq.gulimall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-28 16:33:38
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
