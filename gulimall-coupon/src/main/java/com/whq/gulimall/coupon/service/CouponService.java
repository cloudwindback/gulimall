package com.whq.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.coupon.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-28 16:33:39
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

