package com.whq.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-28 16:33:39
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

