package com.whq.gulimall.coupon.dao;

import com.whq.gulimall.coupon.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-28 16:33:39
 */
@Mapper
public interface SeckillSkuNoticeDao extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
