package com.whq.gulimall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whq.gulimall.product.entity.BrandEntity;
import com.whq.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.security.RunAs;
import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

	@Autowired
	BrandService brandService;
	@Test
	void contextLoads() {
		BrandEntity brandEntity = new BrandEntity();
//		brandEntity.setName("华为");
//		brandService.save(brandEntity);
//		System.out.println("保存成功。。。。。");

//		brandEntity.setBrandId(31L);
//		brandEntity.setDescript("华为品牌");
//		brandService.updateById(brandEntity);
//		System.out.println("更新成功。。。。");

		List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().isNotNull("logo"));
		list.forEach((item)->{
			System.out.println(item);
		});
	}

}
