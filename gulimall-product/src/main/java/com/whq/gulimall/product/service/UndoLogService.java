package com.whq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.product.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-17 14:29:31
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

