package com.whq.gulimall.product.dao;

import com.whq.gulimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-17 14:29:30
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
