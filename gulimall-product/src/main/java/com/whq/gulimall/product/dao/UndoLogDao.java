package com.whq.gulimall.product.dao;

import com.whq.gulimall.product.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-17 14:29:31
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
