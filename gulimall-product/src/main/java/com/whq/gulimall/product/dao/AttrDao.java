package com.whq.gulimall.product.dao;

import com.whq.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-17 14:29:31
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
