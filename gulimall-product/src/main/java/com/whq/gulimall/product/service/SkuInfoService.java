package com.whq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.product.entity.SkuInfoEntity;

import java.util.Map;

/**
 * sku信息
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-17 14:29:30
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

