package com.whq.gulimall.order.dao;

import com.whq.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 14:23:46
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
