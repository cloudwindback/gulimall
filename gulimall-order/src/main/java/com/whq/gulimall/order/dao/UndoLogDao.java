package com.whq.gulimall.order.dao;

import com.whq.gulimall.order.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 14:23:45
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
