package com.whq.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whq.common.utils.PageUtils;
import com.whq.gulimall.order.entity.MqMessageEntity;

import java.util.Map;

/**
 * 
 *
 * @author cloudwindback
 * @email giteeforwhq@163.com
 * @date 2022-11-30 14:23:46
 */
public interface MqMessageService extends IService<MqMessageEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

